import { createApp, h } from 'vue'
import App from './App.vue'
import mydirective from './mydirective.js';

const app = createApp({
    render: () => h(App)
});

app.directive("mydirective", mydirective);
app.mount('#app');
